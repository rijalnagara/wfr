<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//ini_set('display_errors', '1');
//Route::get('/', function () {
//    return view('welcome');
//});
if (App::environment('production')) {
        URL::forceSchema('https');
}
Route::group(['middleware' => ['web']], function () {
        Route::get('/login','SecurityController@login');
        Route::post('/dologin', array('uses' => 'SecurityController@dologin'));
        Route::get('/', ['as' => '/', 'uses' => 'DefaultController@index']);
        
        // menu master
        Route::get('import/data', ['as' => 'import/data', 'uses' => 'Import@data']);
        Route::get('/import/edit/{id}', 'Import@edit');
        Route::get('/import/show/{id}', 'Import@show');
        Route::get('/import/data_detail/{id}', 'Import@data_detail');
        Route::get('/import/generate/{id}', 'Import@generate');
        Route::resource('import', 'Import');
});
Route::get('/servicecheck','SecurityController@check');
Route::get('/servicelogout','SecurityController@logout');

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});
