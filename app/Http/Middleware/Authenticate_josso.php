<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Session;
use App\Simrs\ManAkses\User;
use App\lib\Common;
use Aacotroneo\Saml2\Saml2Auth;
use App\Listeners\EventListenerLogin;
use Saml2;
use URL;


class Authenticate_josso {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth_josso;
        
	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth_josso)
	{
		$this->auth_josso = $auth_josso;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$userData=Session::get('userData');
        if(isset($userData)){
			return $next($request);
        }else{
            return Redirect('/login');
        }
	}



}
