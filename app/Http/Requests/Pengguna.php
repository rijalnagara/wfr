<?php
namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\lib\Common;


class Pengguna extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // extends Validator only for this request
            \Validator::extend( 'composite_unique', function ( $attribute, $value, $parameters, $validator ) {
            // remove first parameter and assume it is the table name
            $table = array_shift( $parameters );
            // start building the conditions
            $fields = [ $attribute => $value ]; // current field, company_code in your case
            // iterates over the other parameters and build the conditions for all the required fields
            while ( $field = array_shift( $parameters ) ) {
                $fields[ $field ] = $this->get( $field );
            }
            // query the table with all the conditions

            $result = \DB::table( $table )->select( \DB::raw( 1 ) )
                ->where( $fields )
                ->where('soft_delete','=','0')
                ->first();

            return empty( $result ); // edited here
        }, 'Pengguna telah ada' );

        if(Common::get_global_session('userDetail','user_level')=='1'){
            $rules=array(
                'app_username' => 'required|composite_unique:man_akses.pengguna,app_username',
                'fak_kd'=>'required'
            );
        }else{
            $rules=array(
                'app_username' => 'required|unique:pgsql.man_akses.pengguna,app_username'
            );
        }
        return $rules;
    }
    public function messages()
    {
        $msg=array(
            'app_username.required' => 'Pengguna harus dipilih',
            'app_username.unique'=>'Pengguna sudah terdaftar',
            'fak_kd.required'=>'Fakultas harus dipilih'
        );
        return $msg;
    }
    public function authorize()
    {
        return true;
    }
}