<?php
namespace App\Http\Requests\Master;

use App\Http\Requests\Request;
use App\lib\Common;
use Lang;

class DoctorRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // extends Validator only for this request
            \Validator::extend( 'composite_unique', function ( $attribute, $value, $parameters, $validator ) {
            // remove first parameter and assume it is the table name
            $table = array_shift( $parameters );
            // start building the conditions
            $fields = [ $attribute => $value ]; // current field, company_code in your case
            // iterates over the other parameters and build the conditions for all the required fields
            while ( $field = array_shift( $parameters ) ) {
                $fields[ $field ] = $this->get( $field );
            }
            // query the table with all the conditions

            $result = \DB::table( $table )->select( \DB::raw( 1 ) )
                ->where( $fields )
                ->whereNull('deleted_at')
                ->first();

            return empty( $result ); // edited here
        }, Lang::get('validation.unique') );

            $rules=array(
                'fnoreg' => 'required|unique:sys_doctor,no_reg',
                 'fname' => 'required',
            );
        return $rules;
    }
    public function messages()
    {
        $msg=array(
            'fnoreg.required' => Lang::get('validation.required'),
            'fname.required' => Lang::get('validation.required'),
        );
        return $msg;
    }
    public function authorize()
    {
        return true;
    }
}