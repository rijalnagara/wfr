<?php
namespace App\Http\Requests;

use App\Http\Requests\Request;


class AnggotaLitabmas extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules=array(
            'pegawai' => 'required','peran' => 'required'
        );
        return $rules;
    }
    public function messages()
    {
        $msg=array(
            'pegawai.required' => 'Nama harus dipilih','peran.required' =>'Peran harus dipilih'
        );
        return $msg;
    }
    public function authorize()
    {
        return true;
    }
}