<?php
namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\lib\Common;
use Lang;


class Login extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules=array(
            'username' => 'required',
            'password'=>'required'
        );
        return $rules;
    }
    public function messages()
    {
        $msg=array(
            'username.required' => Lang::get('auth.username.empty'),
            'password.required'=>Lang::get('auth.password.empty'),
        );
        return $msg;
    }
    public function authorize()
    {
        return true;
    }
}