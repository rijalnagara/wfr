<?php
namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\lib\Common;


class PengajuanInsentif extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // extends Validator only for this request
            \Validator::extend( 'composite_unique', function ( $attribute, $value, $parameters, $validator ) {
            // remove first parameter and assume it is the table name
            $table = array_shift( $parameters );
            // start building the conditions
            $fields = [ $attribute => $value ]; // current field, company_code in your case
            // iterates over the other parameters and build the conditions for all the required fields
            while ( $field = array_shift( $parameters ) ) {
                $fields[ $field ] = $this->get( $field );
            }
            // query the table with all the conditions

            $result = \DB::table( $table )->select( \DB::raw( 1 ) )
                ->where( $fields )
                ->first();

            return empty( $result ); // edited here
        }, 'No surat telah ada' );

        $rules=array(
            'no_surat' => 'required|unique:pgsql.remun_tendik.pengajuan_gaji_pnbp,no_surat',
            'periode' => 'required',
            'tahun'=>'required'
        );
        return $rules;
    }
    public function messages()
    {
        $msg=array(
            'no_surat.required' => 'No surat harus diisi',
            'no_surat.unique'=>'No surat sudah terdaftar',
            'periode.required'=>'Periode harus dipilih',
            'tahun.required'=>'Tahun harus dipilih',
        );
        return $msg;
    }
    public function authorize()
    {
        return true;
    }
}