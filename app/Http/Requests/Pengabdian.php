<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class Pengabdian extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages(){
        $msg=array(
            'id_thn_kegiatan.required' => 'Tahun pengabdian harus dipilih','id_skim.required' =>'Skim kegiatan harus dipilih'
            ,
            // 'id_kel_bidang.required'=>'Kelompok bidang harus dipilih',
            'judul_litabmas.required'=>'Judul harus diisi','lama_kegiatan.required'=>'Lama kegiatan harus dipilih'
            ,'dana_dikti.alpha_num'=>'Dana DIKTI harus diisi dengan angka','dana_pt.alpha_num'=>'Dana PT harus diisi dengan angka'
            ,'dana_institusi_lain.alpha_num'=>'Dana institusi lain harus diisi dengan angka','output_penelitian.required'=>'Output pengabdian harus dipilih'
            ,'status.required'=>'Status harus dpilih'
        );
        return $msg;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         $rules=array(
            'id_thn_kegiatan' => 'required',
            'id_skim' => 'required',
            // 'id_kel_bidang'=>'required',
            'judul_litabmas'=>'required'
            ,'lama_kegiatan'=>'required',
            'dana_dikti'=>'alpha_num',
            'dana_pt'=>'alpha_num',
            'dana_institusi_lain'=>'alpha_num'
            ,'output_penelitian'=>'required',
            'status'=>'required'
        );
        return $rules;
    }
}
