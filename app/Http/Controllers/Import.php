<?php

namespace App\Http\Controllers;

use App\lib\Common;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Wrf\Data\Import as ImportModel;
use App\Http\Requests\ImportRequest;
use App\Wrf\Data\Activity as ActivityModel;
use App\Wrf\Data\Sequence as SequenceModel;
use Lang;
use Session;
use View;
use Validator;
use Input;
use Yajra\Datatables\Datatables;
use SimpleXMLElement;
use GraphAware\Neo4j\Client\ClientBuilder;

class Import extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $menu=array('menu'=>'menu.import','submenu'=>'','hakAkses'=>Session::get('userRole'),'userId'=>Session::get('userID'),'jml_data'=>'');
        return View::make('import/index')->with('menu',$menu);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $menu=array('menu'=>'menu.import.create','submenu'=>'','hakAkses'=>Session::get('userRole'),'userId'=>Session::get('userID')
        ,'jml_data'=>'');
        return View::make('import/create')->with('menu',$menu);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ImportRequest $request)
    {
        //load activity
        $xml_parse=simplexml_load_file($_FILES['fbpmn']['tmp_name']);
        $xml_parse->registerXPathNamespace( 'bpmn2', 'http://www.omg.org/spec/BPMN/20100524/MODEL' );
        //$start_outgoing=$xml_parse->xpath('.//bpmn2:process/bpmn2:task[@id="el_2691174925ca1955b379cd8016889263"]/bpmn2:incoming');
        //$start_outgoing=$xml_parse->attributes("id","el_2691174925ca1955b379cd8016889263");
        $id_process=Common::gen_uuid();
        $import=new ImportModel();
        $import->id=$id_process;
        $import->name=$_FILES['fbpmn']['name'];
        if($import->save()){
            //load activity
            $xml_parse=simplexml_load_file($_FILES['fbpmn']['tmp_name']);
            $xml_parse->registerXPathNamespace( 'bpmn2', 'http://www.omg.org/spec/BPMN/20100524/MODEL' );
            $data=$xml_parse->xpath('//bpmn2:process');
            //$exGat=$xml_parse->xpath('.//bpmn2:process/bpmn2:exclusiveGateway/bpmn2:incoming');
            //dd($exGat->attributes());
            foreach ($data as $datas) {
                // insert start event
                $start=$xml_parse->xpath('.//bpmn2:process/bpmn2:startEvent');
                foreach ($start as $st => $val_st){
                    $activity=new ActivityModel();
                    $activity->id=Common::gen_uuid();
                    $activity->id_process=$id_process;
                    if($val_st->attributes()->{'name'}->{0}){
                        $activity->name=$val_st->attributes()->{'name'}->{0};
                    }else{
                        $activity->name='Start Event';
                    }
                    $activity->source=$val_st->attributes()->{'id'}->{0};
                    $activity->created_at=date('Y-m-d H:i:s');
                    $activity->category='1';
                    $activity->save();
                }
                $process_name=$xml_parse->xpath('.//bpmn2:process/bpmn2:task');
                foreach ($process_name as $name => $val) {
                    $id_task = Common::gen_uuid();
                    $id_source = $val->attributes()->{'id'}->{0};
                    $task_name = $val->attributes()->{'name'}->{0};
                    //print $id_source;
                    $activity = new ActivityModel();
                    $activity->id = $id_task;
                    $activity->id_process = $id_process;
                    $activity->name = $task_name;
                    $activity->source = $id_source;
                    $activity->created_at = date('Y-m-d H:i:s');
                    $activity->category = '2';
                    $activity->save();
                    /*if($activity->save()){
                        $start_incoming=$xml_parse->xpath('.//bpmn2:process/bpmn2:task[@id="'.$id_source.'"]/bpmn2:incoming');
                        //dd($start_incoming);
                        foreach ($start_incoming as $start_incoming_task => $val_start_incoming){
                            print $val_start_incoming->{0}."<br>";
                            $activity_in=new ActivityModel();
                            $activity_in->id=Common::gen_uuid();
                            $activity_in->id_process=$id_process;
                            $activity_in->name=$task_name;
                            $activity_in->source=$id_source;
                            $activity_in->created_at=date('Y-m-d H:i:s');
                            $activity_in->category='3';
                            if(!empty($val_start_incoming->{0})){
                                $activity_in->in_out=$val_start_incoming->{0};
                                $activity_in->save();
                            }
                        }
                        $start_outgoing=$xml_parse->xpath('.//bpmn2:process/bpmn2:task[@id="'.$id_source.'"]/bpmn2:outgoing');
                        //dd($start_incoming);
                        foreach ($start_outgoing as $start_outgoing_task => $val_start_outgoing){
                            $activity_out=new ActivityModel();
                            $activity_out->id=Common::gen_uuid();
                            $activity_out->id_process=$id_process;
                            $activity_out->name=$task_name;
                            $activity_out->source=$id_source;
                            $activity_out->created_at=date('Y-m-d H:i:s');
                            $activity_out->category='4';
                            if(!empty($val_start_outgoing->{0})){
                                $activity_out->in_out=$val_start_outgoing->{0};
                                $activity_out->save();
                            }
                        }*/
                }
                $eg_name=$xml_parse->xpath('.//bpmn2:process/bpmn2:exclusiveGateway');
                foreach ($eg_name as $egs_name => $eg_val) {
                    $id_task = Common::gen_uuid();
                    $id_source = $eg_val->attributes()->{'id'}->{0};
                    $task_name = $eg_val->attributes()->{'name'}->{0};
                    //print $id_source;
                    $activity = new ActivityModel();
                    $activity->id = $id_task;
                    $activity->id_process = $id_process;
                    $activity->name = $task_name;
                    $activity->source = $id_source;
                    $activity->created_at = date('Y-m-d H:i:s');
                    $activity->category = '3';
                    $activity->save();
                }
                $sp_name=$xml_parse->xpath('.//bpmn2:process/bpmn2:subProcess');
                foreach ($sp_name as $sps_name => $sp_val) {
                    $id_task = Common::gen_uuid();
                    $id_source = $sp_val->attributes()->{'id'}->{0};
                    $task_name = $sp_val->attributes()->{'name'}->{0};
                    //print $id_source;
                    $activity = new ActivityModel();
                    $activity->id = $id_task;
                    $activity->id_process = $id_process;
                    $activity->name = $task_name;
                    $activity->source = $id_source;
                    $activity->created_at = date('Y-m-d H:i:s');
                    $activity->category = '4';
                    $activity->save();
                }
                $end=$xml_parse->xpath('.//bpmn2:process/bpmn2:endEvent');
                foreach ($end as $et => $val_et){
                    $activity=new ActivityModel();
                    $activity->id=Common::gen_uuid();
                    $activity->id_process=$id_process;
                    if($val_et->attributes()->{'name'}->{0}){
                        $activity->name=$val_et->attributes()->{'name'}->{0};
                    }else{
                        $activity->name='End Event';
                    }
                    $activity->source=$val_et->attributes()->{'id'}->{0};
                    $activity->created_at=date('Y-m-d H:i:s');
                    $activity->category='1';
                    $activity->save();
                }
                $activitymodel=new ActivityModel();
                $seq=$xml_parse->xpath('.//bpmn2:process/bpmn2:sequenceFlow');
                //dd($seq);
                foreach ($seq as $sq => $val_sq){
                        $sourceRef = $activitymodel::get_data($val_sq->attributes()->{'sourceRef'}->{0}, $id_process);
                        $targetRef = $activitymodel::get_data($val_sq->attributes()->{'targetRef'}->{0}, $id_process);
                        //cek jika exclusivegateway
                        if($targetRef['0']->category==3){
                            $target_eg=$xml_parse->xpath('.//bpmn2:process/bpmn2:sequenceFlow[@sourceRef="'.$val_sq->attributes()->{'targetRef'}->{0}.'"]');
                            foreach ($target_eg as $target_egs => $val_target_egs){
                                $targetRef_eg = $activitymodel::get_data($val_target_egs->attributes()->{'targetRef'}->{0}, $id_process);
                                $tabseq = new SequenceModel();
                                $tabseq->id = Common::gen_uuid();
                                if (!empty($sourceRef['0']) && !empty($targetRef_eg['0'])) {
                                    $tabseq->id_source_activity = $sourceRef['0']->id;
                                    $tabseq->id_target_activity = $targetRef_eg['0']->id;
                                    //dd($tabseq);
                                    $tabseq->save();
                                }
                            }
                        }else {
                            $tabseq = new SequenceModel();
                            $tabseq->id = Common::gen_uuid();
                            if (!empty($sourceRef['0']) && !empty($targetRef['0'])) {
                                $tabseq->id_source_activity = $sourceRef['0']->id;
                                $tabseq->id_target_activity = $targetRef['0']->id;
                                //dd($tabseq);
                                $tabseq->save();
                            }
                        }
                }

            }
        }
        $request->session()->flash('status',Lang::get('system.save.success',array(),config('app.locale')));
        return redirect()->route('import.index');
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $menu=array('menu'=>'menu.import.detail','submenu'=>'','hakAkses'=>Session::get('userRole'),'userId'=>Session::get('userID'),'jml_data'=>'','id'=>$id);
        return View::make('import/detail')->with('menu',$menu);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function generate($id)
    {
        //
        try {
            //create connection
            $client = ClientBuilder::create()
                ->addConnection(getenv('GRAPH_NAME'),getenv('GRAPH_HOST'))// Example for HTTP connection configuration (port is optional)
                ->build();
            $data=ImportModel::get_list($id);
            foreach ($data as $datas){
                //dd($datas);
                $query='Merge (:Activity{Name:"'.$datas['activity_name'].'",id:"'.$datas['id_activity'].'",id_sub:"'.$datas['in_out'].'"})';
                $client->run($query);
            }
            $data_seq=SequenceModel::get_list($id);
            foreach ($data_seq as $datas_seq){
                $stack_relationship = $client->stack();
                $stack_relationship->push('MATCH (e1:Activity {id: "' . $datas_seq['id_source_activity'] . '"})
                         ,(e2:Activity {id:"' . $datas_seq['id_target_activity'] . '"})
                         where size((e1)-->(e2))=0 create (e1)-[next:Next]->(e2)');
                $resultSet = $client->runStack($stack_relationship);

                //create XORSPLIT
                $query_xorsplit = $client->stack();
                $query_xorsplit->push('match (n:Activity)-[r:Next]->(a:Activity)
where size ((n)-->()) >1 and size((a)<--()) = 1 create (n)-[:XORSPLIT]->(a) 
                DELETE r');
                $resultSet = $client->runStack($query_xorsplit);

                //create XORJOIN
                $query_xorjoin = $client->stack();
                $query_xorjoin->push('match (n:Activity)-[r:Next]->(a:Activity)
where size ((n)-->()) =1 and size((a)<--()) >1 create (n)-[:XORJOIN]->(a) 
                DELETE r');
                $resultSet = $client->runStack($query_xorjoin);
            }
            //ganti label subprocess
            $sub=ImportModel::get_subprocess($id);
            foreach ($sub as $datas_sub){
                $stack_relationship = $client->stack();
                $stack_relationship->push('MATCH (n:Activity {Name:"'.$datas_sub['activity_name'].'"}) 
                remove n:Activity set n:SubProcess');
                $resultSet = $client->runStack($stack_relationship);
            }

        }catch (\GraphAware\Neo4j\Client\Exception\Neo4jException $m){
           dd($m);
        }
        Session::flash('status',Lang::get('system.save.success',array(),config('app.locale')));
        return redirect()->route('import.index');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data=ImportModel::get_list_activity($id);
        $menu=array('menu'=>'Tambah Sub Proses','submenu'=>'','hakAkses'=>Session::get('userRole'),'userId'=>Session::get('userID')
        ,'id'=>$id,'data'=>$data);
        return View::make('import/edit')->with('menu',$menu);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $import=new ActivityModel();
        $import=$import->find(Input::get('activity'));
        $import->in_out=$id;
        $import->save();
        Session::flash('status',Lang::get('system.save.success',array(),config('app.locale')));
        return redirect()->route('import.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function data(){
        $query=ImportModel::all();
        $query=collect($query);
        return Datatables::of($query)
            ->addColumn('aksi', function($row) {
                return '<a href="'.url('/import/show')."/".$row->id.'" class="btn btn-success"><i class="fas fa-search"></i> Lihat Aktivitas</a>
                <a href="'.url('/import/generate')."/".$row->id.'" class="btn btn-warning"><i class="fas fa-upload"></i> Buat GraphDB</a>
                ';
            })
            ->filterColumn('name', function($query, $keyword) {
                $query->havingRaw('lower(name) like ?', ["%{$keyword}%"]);
            })
            ->make(true);
    }

    public function data_detail($id){
        $query=ImportModel::get_list($id);
        $query=collect($query);
        return Datatables::of($query)
            ->filterColumn('name', function($query, $keyword) {
                $query->havingRaw('lower(name) like ?', ["%{$keyword}%"]);
            })
            ->filterColumn('activity_name', function($query, $keyword) {
                $query->havingRaw('lower(activity_name) like ?', ["%{$keyword}%"]);
            })
            ->addColumn('action', function($row) {
                if($row['category_name']=='4'){
                    return "<a href=".url('/import/edit')."/".$row['id_activity']." class='btn btn-success'><i class='fas fa-plus'></i> Pilih Sub Proses</a>";
                }else{
                    return "-";
                }
            })
            ->make(true);
    }
}
