<?php
namespace App\Http\Controllers;
use App\Http\Requests\Login;
use Closure;
use App\Http\Controllers\Controller;
use App\Http\Middleware\Authenticate_josso;
use Illuminate\Support\Facades\Request;
use View;
use Session;
use App\Wrf\ManAkses\User;
use App\lib\Common;
use Validator;
use Lang;
class SecurityController extends Controller {

    public static function login(){
        return View::make('auth/login');
    }

    public static function dologin(Login $request){
        $validator=Validator::make(\Input::all(),$request->rules(),$request->messages());
        if($validator->fails()){
            return redirect('/login')->withErrors($validator)->withInput();
        }
        $user=new User();
        $user=$user->getUser(\Input::get('username'),md5(\Input::get('password')));
        if(isset($user)){
            session::put('userData',$user);
            return redirect('/import');
        }else{
            return redirect('/login')->withErrors(Lang::get('auth.failed'))->withInput();
        }
    }
	/*
	|--------------------------------------------------------------------------
	| Profil Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/
    public static function logout()
    {
        // Session::put('ketDosen',' ');
        session_start();
        Session::flush();
        if (isset($_SERVER['HTTP_COOKIE'])) {
            $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
            foreach($cookies as $cookie) {
                $parts = explode('=', $cookie);
                $name = trim($parts[0]);
                setcookie($name, '', time()-1000);
                setcookie($name, '', time()-1000, '/');
            }
        }

        // Unset all of the session variables.
        $_SESSION = array();

        // If it's desired to kill the session, also delete the session cookie.
        // Note: This will destroy the session, and not just the session data!
        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
        }

        // Finally, destroy the session.
        session_destroy();
        Saml2::logout();

        // return redirect()('/');

    }

    public static function change_access($as,\Illuminate\Http\Request  $request){
        if($as=='tendik'){
            $user_level='0';
            $data_user['user_level']=$user_level;
            $data_user['nm_pengguna']=\App\lib\Common::get_global_session('userID');
            $data_user['fak_kd']='-';
            $data_user['a_aktif']='1';
            Common::check_access($user_level);
            Session::put('userID', \App\lib\Common::get_global_session('userID'));
            Session::put('userDetail',$data_user);
            Session::put('change','tendik');
            return redirect('pengajuan_insentif');
        }else{
            Session::put('change','');
            $user_x=new User;
            $data_user=$user_x->getUser(\App\lib\Common::get_global_session('userID'));
            if(empty($data_user)){
                $user_level='0';
                $data_user['user_level']=$user_level;
                $data_user['nm_pengguna']=\App\lib\Common::get_global_session('userID');
                $data_user['fak_kd']='-';
            }else{
                $user_level=$data_user->user_level;
            }
            Common::check_access($user_level);
            Session::put('userID', \App\lib\Common::get_global_session('userID'));
            Session::put('userDetail',$data_user);
            return redirect()->back()->withSuccess('Hak akses menjadi Admin');
        }

    }

}
