<?php

namespace App\Remun\Pegawai;

use Illuminate\Database\Eloquent\Model;
use DB;

class Pegawai extends \Eloquent
{
    protected $table = 'pegawai.pegawai as a';
    protected $primaryKey = 'id';
    protected $fillable = ['_token','id'];
    public $timestamps = false;

    public function get_pegawai($id_unit,$search='',$tahun=''){
        DB::enableQueryLog();
        if(!$tahun){
            $tahun=date('Y')-1;
        }
        $pegawai=new Pegawai;
        //jika bakpik ato buk
        if($id_unit=='1000' || $id_unit=='2000'){
            $sub_id=substr($id_unit,0,1);
        }else{
            $sub_id=substr($id_unit,0,2);
        }
        $pegawai=$pegawai->addSelect('a.nip','nama_pegawai','nilai')
                ->leftJoin('remun_tendik.nilai_skp as b',function($leftJoin) use ($tahun)
                {
                    $leftJoin->on('a.nip', '=', 'b.nip')->where('tahun', '=', $tahun);


                })
                ->where('kode_unit','like',$sub_id."%")
                ->where('status_pegawai','=','2')
                ->where('id_jns_pegawai','!=','5')
                ->orderBy('nama_pegawai')
                ->get()
                ->toArray();
        //dd(DB::getQueryLog());exit;
        return $pegawai;
    }

    public function get_pegawai_presensi($id_unit,$bulan){
        DB::enableQueryLog();
        $pegawai=new Pegawai;
        //jika bakpik ato buk
        if($id_unit=='1000' || $id_unit=='2000'){
            $sub_id=substr($id_unit,0,1);
        }else{
            $sub_id=substr($id_unit,0,2);
        }
        $pegawai=$pegawai->addSelect('nama_pegawai','b.*','a.nip')
            ->leftJoin('remun_tendik.presensi as b',function($leftJoin) use ($bulan)
            {
                $leftJoin->on('a.nip', '=', 'b.nip')
                         ->where('tahun', '=',date("Y") )
                         ->where('bulan', '=',$bulan);
            })
            ->where('kode_unit','like',$sub_id."%")
            ->where('status_pegawai','=','2')
            ->where('id_jns_pegawai','!=','5')
            ->orderBy('nama_pegawai')
            ->get()
            ->toArray();
        //dd(DB::getQueryLog());exit;
        //dd($pegawai);exit;
        return $pegawai;
    }

    public function get_pegawai_surattugas($id_unit,$tahun){
        //DB::enableQueryLog();
        $pegawai=new Pegawai;
        //jika bakpik ato buk
        if($id_unit=='1000' || $id_unit=='2000'){
            $sub_id=substr($id_unit,0,1);
        }else{
            $sub_id=substr($id_unit,0,2);
        }
        $pegawai=$pegawai->addSelect('nama_pegawai','b.*','a.nip')
            ->leftJoin('remun_tendik.surat_tugas as b',function($leftJoin) use ($tahun)
            {
                $leftJoin->on('a.nip', '=', 'b.nip')->where('tahun', '=', $tahun );


            })
            ->where('kode_unit','like',$sub_id."%")
            ->where('status_pegawai','=','2')
            ->where('id_jns_pegawai','!=','5')
            ->orderBy('nama_pegawai')
            ->get()
            ->toArray();
        //dd(DB::getQueryLog());exit;
        return $pegawai;
    }

    public static function rekap_nilai($kode,$tahun=''){
        DB::enableQueryLog();
        if(!$tahun){
            $tahun=date('Y')-1;
        }
        $pegawai=new Pegawai;
        //jika bakpik ato buk
        if($id_unit=='1000' || $id_unit=='2000'){
            $sub_id=substr($id_unit,0,1);
        }else{
            $sub_id=substr($id_unit,0,2);
        }
        //dd($kode);
        $query1=$pegawai->select(array(DB::raw('count(a.nip) as jml'),DB::raw('1 as urut')))
            ->leftJoin('remun_tendik.nilai_skp as b',function($leftJoin) use ($tahun)
            {
                $leftJoin->on('a.nip', '=', 'b.nip')->where('tahun', '=', $tahun);


            })
            ->where('kode_unit','like',$sub_id."%")
            ->where('status_pegawai','=','2')
            ->where('id_jns_pegawai','!=','5')
            ->where('nilai','=','0');

        //dd($query1);
        $query2=$pegawai->select(array(DB::raw('count(a.nip) as jml'),DB::raw('2 as urut')))
            ->leftJoin('remun_tendik.nilai_skp as b',function($leftJoin) use ($tahun)
            {
                $leftJoin->on('a.nip', '=', 'b.nip')->where('tahun', '=', $tahun );


            })
            ->where('kode_unit','like',$sub_id."%")
            ->where('status_pegawai','=','2')
            ->where('id_jns_pegawai','!=','5')
            ->where('nilai','>','0')
                ->where('nilai','<=','50');
        $query3=$pegawai->select(array(DB::raw('count(a.nip) as jml'),DB::raw('3 as urut')))
            ->leftJoin('remun_tendik.nilai_skp as b',function($leftJoin) use ($tahun)
            {
                $leftJoin->on('a.nip', '=', 'b.nip')->where('tahun', '=', $tahun );


            })
            ->where('kode_unit','like',$sub_id."%")
            ->where('status_pegawai','=','2')
            ->where('id_jns_pegawai','!=','5')
            ->where('nilai','>','51')
            ->where('nilai','<=','80');
        $query4=$pegawai->select(array(DB::raw('count(a.nip) as jml'),DB::raw('4 as urut')))
            ->leftJoin('remun_tendik.nilai_skp as b',function($leftJoin) use ($tahun)
            {
                $leftJoin->on('a.nip', '=', 'b.nip')->where('tahun', '=', $tahun );


            })
            ->where('kode_unit','like',$sub_id."%")
            ->where('status_pegawai','=','2')
            ->where('id_jns_pegawai','!=','5')
            ->where('nilai','>','80');
        $data=$query1->union($query2)->union($query3)->union($query4)
                ->orderBy('urut')
                ->get()
                ->toArray();
        //dd($data);
        return $data;
    }
    public static function get_data_pegawai($nip){
        $data=DB::select("SELECT
  pegawai.nip,
  pegawai.nama_pegawai,
  pegawai.gelar_depan,
  pegawai.gelar_belakang,
  golongan_pangkat.golongan,
  golongan_pangkat.pangkat,
  jenis_jabatan_uj_remun2016.id_jabatan_uj,
  jenis_jabatan_uj_remun2016.nama_jabatan_uj,
  jenis_jabatan_uj_remun2016.job_class,
  jv_rendah,
  jv_rendah*2984 as idr,
  pegawai.id_jabatan_uj
FROM
  pegawai.pegawai,
  pegawai.jenis_jabatan_uj_remun2016,
  pegawai.golongan_pangkat
WHERE
  pegawai.id_golpangkat_terakhir = golongan_pangkat.id_golpangkat AND
  pegawai.id_jabatan_uj = jenis_jabatan_uj_remun2016.id_jabatan_uj AND nip='$nip';");
        return $data;
    }
}
