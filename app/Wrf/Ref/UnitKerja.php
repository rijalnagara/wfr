<?php

namespace App\Remun\Ref;

use Illuminate\Database\Eloquent\Model;

class UnitKerja extends \Eloquent
{
    protected $table = 'pegawai.unit_kerja';

    public static function get_list(){
        return self::select(\DB::raw("nama_unit as urai"))
            ->addSelect("unit_kerja.kode_unit as id")
            ->addSelect(\DB::raw('COUNT(nip) as jml'))
            ->Join('pegawai.pegawai','pegawai.kode_unit_induk','=','unit_kerja.kode_unit')
            ->where('tingkat','=','1')
            ->where('unit_kerja.kode_unit','!=','0000')
            ->groupBy('nama_unit')
            ->groupBy('unit_kerja.kode_unit')
            ->orderBy('unit_kerja.kode_unit')
            ->lists('urai','id','jml')
            ->toArray();
    }
    public static function get_unit($id){
        return self::select(\DB::raw("nama_unit as urai"))
            ->where('kode_unit','=',$id)
            ->lists('urai')
            ->toArray();
    }

}
