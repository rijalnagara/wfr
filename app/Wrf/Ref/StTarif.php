<?php

namespace App\Remun\Ref;

use Illuminate\Database\Eloquent\Model;

class StTarif extends \Eloquent
{
    protected $table = 'remun_tendik.st_tarif';

    public static function get_list(){
        return self::select('jabatan_tugas', 'nominal')
            ->whereNotNull('jabatan_tugas')
            ->orderBy('jabatan_tugas')
            ->lists('nominal', 'jabatan_tugas')
            ->toArray();
    }

}
