<?php

namespace App\Remun\Ref;

use Illuminate\Database\Eloquent\Model;

class PresensiFtm extends \Eloquent
{
  protected $connection = 'mysql';

  public function get_log_presensi($nip, $month, $year, $holidays) {
    $t_att_log = ($year != date('Y')) ? "att_log_".$year : "att_log";
    
    return \DB::connection('mysql')->select(\DB::raw("SELECT *, DAYOFWEEK(tgl) dow FROM
    (SELECT nik, DATE(scan_date) tgl, (CASE WHEN MIN(TIME(scan_date)) < TIME('11:00:00') THEN MIN(TIME(scan_date)) ELSE NULL END) jm_msk
    FROM $t_att_log INNER JOIN emp ON emp.pin = $t_att_log.pin
    WHERE TRIM(nik) IN ($nip) AND MONTH(scan_date) = $month AND YEAR(scan_date) = $year
    GROUP BY nik, tgl) a
    NATURAL JOIN
    (SELECT nik, DATE(scan_date) tgl, (CASE WHEN MAX(TIME(scan_date)) > TIME('11:00:00') THEN MAX(TIME(scan_date)) ELSE NULL END) jm_plg
    FROM $t_att_log INNER JOIN emp ON emp.pin = $t_att_log.pin
    WHERE TRIM(nik) IN ($nip) AND MONTH(scan_date) = $month AND YEAR(scan_date) = $year
    GROUP BY nik, tgl) b
    WHERE DAY(tgl) NOT IN ($holidays)
    ORDER BY nik ASC, tgl ASC, dow ASC
    "));
  }

  public function get_holidays($month, $year) {
    return \DB::connection('mysql')->table('holiday')
    ->select('holiday_date')
    ->whereRaw("MONTH(holiday_date) = '".$month."'")
    ->whereRaw("YEAR(holiday_date) = '".$year."'")
    ->orderBy('holiday_date')
    ->lists('holiday_date');
  }

}
