<?php

namespace App\Simrs\ref;

use Illuminate\Database\Eloquent\Model;

class specialist extends Model
{
    //
    protected $table = 'sys_specialist';
    protected $primaryKey='id';
    protected $fillable = ['_token','name'];
    public $timestamps = false;
    public $incrementing=false;
}
