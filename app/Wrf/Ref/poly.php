<?php

namespace App\Simrs\ref;

use Illuminate\Database\Eloquent\Model;

class poly extends \Eloquent
{
    //
    protected $table = 'sys_poly';
    protected $primaryKey='id';
    protected $fillable = ['_token','name'];
    public $timestamps = false;
    public $incrementing=false;
}
