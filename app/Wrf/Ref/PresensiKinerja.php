<?php

namespace App\Remun\Ref;

use Illuminate\Database\Eloquent\Model;

class PresensiKinerja extends \Eloquent
{
    // protected $table = 'skp.t_presensi_'.date("Y");

  public static function get_ijin_presensi($nip='', $bulan='', $tahun='') {
    $result = [];
    if($bulan && $tahun) {
      $db = \DB::table('skp.t_presensi_'.$tahun)
      ->select('nip', 'tgl_presensi', 'status_presensi', 'kategori_presensi')
      ->whereRaw("EXTRACT(MONTH FROM tgl_presensi) = '".$bulan."'")
      ->whereRaw("EXTRACT(YEAR FROM tgl_presensi) = '".$tahun."'");

      if(is_array($nip)) {
        $db->whereIn('nip', $nip);
      } 
      else {
        $db->where('nip', '=', $nip);
      }
      $db->orderBy('nip');
      $db->orderBy('tgl_presensi');
      $result = $db->get();
    }
    return $result;
  }

  public static function get_jadwal_ramadhan($tahun='') {
    return \DB::table('skp.t_mode_jamkerja')
      ->where('mode', '=', 'ramadhan')
      ->where('tahun', '=', $tahun)
      ->first();
  }


}
