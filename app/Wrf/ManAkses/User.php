<?php

namespace App\Wrf\ManAkses;
use Illuminate\Database\Eloquent\Model;
use DB;


class User extends \Eloquent
{
    protected $primaryKey='id';
    protected $table='sys_user';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','departement',
    ];

    public function getUser($username,$passwd){
        $user=new User;
        $user=$user->where('username','=',$username)
                ->where('passwd','=',$passwd)
                ->whereNull('deleted_at')
                ->first();
        return $user;
    }



}
