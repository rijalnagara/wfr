<?php

namespace App\Wrf\Data;

use Illuminate\Database\Eloquent\Model;
use DB;

class Activity extends \Eloquent
{
    //
    protected $table = 'sys_activity';
    protected $primaryKey='id';
    protected $fillable = ['_token','name'];
    public $timestamps = false;
    public $incrementing=false;

    public static function get_data($source,$id_process){
        DB::enableQueryLog();
        $activity=new Activity();
        $data=$activity->addSelect('id')
            ->addSelect('category')
            ->where('source','=',$source)
            ->where('id_process','=',$id_process)
            ->get();
        return $data;
        //dd(DB::getQueryLog());exit;
    }
}
