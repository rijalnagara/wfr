<?php

namespace App\Wrf\Data;

use Illuminate\Database\Eloquent\Model;

class Sequence extends \Eloquent
{
    //
    protected $table = 'sys_sequence';
    protected $primaryKey='id';
    protected $fillable = ['_token','address'];
    public $timestamps = false;
    public $incrementing=false;

    public static function get_list($id){
        //DB::enableQueryLog();
        $sequence=new Sequence();
        $data=$sequence->addSelect("id_source_activity")
            ->addSelect("id_target_activity")
            ->Join('sys_activity','sys_activity.id','=','id_source_activity')
            ->whereIn('category',array('2','4'))
            ->where('id_process','=',$id)
            ->orderBy('source')
            ->get()
            ->toArray();
        return $data;
        //dd(DB::getQueryLog());exit;
    }
}
