<?php

namespace App\Wrf\Data;

use Illuminate\Database\Eloquent\Model;
use DB;

class Import extends \Eloquent
{
    //
    protected $table = 'sys_process';
    protected $primaryKey='id';
    protected $fillable = ['_token','in_out'];
    public $timestamps = false;
    public $incrementing=false;

    public static function get_list($id){
        DB::enableQueryLog();
        $import=new Import();
        $data=$import->addSelect("sys_process.name")
            ->addSelect("sys_activity.id as id_activity")
            ->addSelect("sys_activity.name as activity_name")
            ->addSelect("sys_activity.in_out as in_out")
            ->addSelect("category as category_name")
            ->Join('sys_activity','id_process','=','sys_process.id')
            ->whereIn('category',array('2','4'))
            ->where('sys_process.id','=',$id)
            ->orderBy('source')
            ->get()
            ->toArray();
        //dd(DB::getQueryLog());exit;
        return $data;

    }

    public static function get_list_activity($id){
        DB::enableQueryLog();
        $import=new Import();
        $data=$import->addSelect("sys_process.name")
            ->addSelect("sys_activity.id as id_activity")
            ->addSelect("sys_activity.name as activity_name")
            ->addSelect("category as category_name")
            ->Join('sys_activity','id_process','=','sys_process.id')
            ->whereIn('category',array('2'))
            ->orderBy('source')
            ->get()
            ->toArray();
        //dd(DB::getQueryLog());exit;
        return $data;

    }

    public static function get_subprocess($id){
        DB::enableQueryLog();
        $import=new Import();
        $data=$import->addSelect("sys_process.name")
            ->addSelect("sys_activity.id as id_activity")
            ->addSelect("sys_activity.name as activity_name")
            ->addSelect("category as category_name")
            ->Join('sys_activity','id_process','=','sys_process.id')
            ->where('category','=','4')
            ->where('sys_process.id','=',$id)
            ->orderBy('source')
            ->get()
            ->toArray();
        //dd(DB::getQueryLog());exit;
        return $data;

    }
}
