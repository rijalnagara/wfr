@extends('layout.login')
@section('content')
	<div id="loginbox">
		{!! Form::open(array('url' => 'dologin','class' => 'form_vertical','method'=>'post')) !!}
		{{csrf_field()}}
			<div class="control-group normal_text"> <h3><img src="img/logo.png" alt="Logo" /></h3></div>
        @include('layout.error')
			<div class="control-group">
				<div class="controls">
					<div class="main_input_box">
						<span class="add-on bg_lg"><i class="icon-user"> </i></span><input type="text" placeholder="{{ Lang::get('auth.username') }}" name="username" value="{{ old('username') }}"/>
					</div>
				</div>
			</div>
			<div class="control-group">
				<div class="controls">
					<div class="main_input_box">
						<span class="add-on bg_ly"><i class="icon-lock"></i></span><input type="password" placeholder="{{ Lang::get('auth.password') }}" name="password"/>
					</div>
				</div>
			</div>
			<div class="form-actions">
				<span class="pull-left"><a href="#" class="flip-link btn btn-info" id="to-recover">Lost password?</a></span>
				<span class="pull-right"><input type="submit" class="btn btn-success" value="{{ Lang::get('auth.login') }}"/></span>
			</div>
		{!! Form::close() !!}
	</div>
@endsection
