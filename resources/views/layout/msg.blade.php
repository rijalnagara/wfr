@if (session('status'))
    <div class="alert alert-success alert-dismissible">
        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
        <h4>
            <i class="icon fa fa-check"></i>
            Info!
        </h4>
        {{ session('status') }}
    </div>
    <br>
@endif