<!DOCTYPE html>
<html lang="en">

<head>
  <title>WORKFLOW REPOSITORY</title><meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <?php if(App::environment()=='production'){ ?>
    <link rel="stylesheet" href="{{secure_asset("css/bootstrap.min.css") }}" />
    <link rel="stylesheet" href="{{secure_asset("css/bootstrap-responsive.min.css") }}" />
    <link rel="stylesheet" href="{{secure_asset("css/matrix-login.css") }}" />
    <link href="{{secure_asset("font-awesome/css/font-awesome.css") }}" rel="stylesheet" />
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
  <?php }else{?>
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/bootstrap-responsive.min.css" />
    <link rel="stylesheet" href="css/matrix-login.css" />
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
  <?php } ?>
</head>
<body>
          @yield('content')

          <script src="js/jquery.min.js"></script>
          <script src="js/matrix.login.js"></script>
</body>

</html>