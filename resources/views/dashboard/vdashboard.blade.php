@extends('layout.master',['menu'=>$menu])
@section('content')
    <!-- Main content -->
        <section class="content">
              <!-- Small boxes (Stat box) -->
          <div class="row">
                  <div class="col-lg-4 col-xs-8">
                      <div class="small-box bg-aqua">
                          <div class="inner">
                              <h3>150</h3>
                              <p>Draf Pengajuan Gaji PNBP</p>
                          </div>
                          <div class="icon">
                              <i class="ion ion-bag"></i>
                          </div>
                          <a class="small-box-footer" href="#">
                              More info
                              <i class="fa fa-arrow-circle-right"></i>
                          </a>
                      </div>
                  </div>

                      <div class="col-lg-4 col-xs-8">
                          <div class="small-box bg-green">
                              <div class="inner">
                                  <h3>150</h3>
                                  <p>Pengajuan Gaji PNBP</p>
                              </div>
                              <div class="icon">
                                  <i class="ion ion-bag"></i>
                              </div>
                              <a class="small-box-footer" href="#">
                                  More info
                                  <i class="fa fa-arrow-circle-right"></i>
                              </a>
                          </div>
                      </div>
                      <div class="col-lg-4 col-xs-8">
                          <div class="small-box bg-yellow">
                              <div class="inner">
                                  <h3>150</h3>
                                  <p>Pengajuan Gaji PNBP diproses</p>
                              </div>
                              <div class="icon">
                                  <i class="ion ion-bag"></i>
                              </div>
                              <a class="small-box-footer" href="#">
                                  More info
                                  <i class="fa fa-arrow-circle-right"></i>
                              </a>
                          </div>
                      </div>

          </div><!-- /.row -->
        </section><!-- /.content -->
@endsection
