@extends('layout.master',['menu'=>$menu])
@section('content')
        <!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">

    <!-- ============================================================== -->
    <!-- Data table -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                {!! Form::open(array('route' => array('import.update',$menu['id']),'class' => 'form_horizontal','method' => 'PATCH','id'=>'form')) !!}
                {{csrf_field()}}
                    <div class="card-body">
                        @include('layout.error')
                        <div class="form-group row">
                            <label for="fbpmn" class="col-sm-2 control-label col-form-label">Aktivitas</label>
                            <div class="col-sm-10">
                                    <select name="activity" class="custom-input">
                                        <option value="">--Pilih--</option>
                                        @foreach($menu['data'] as $datas )
                                            <option value="{{ $datas['id_activity'] }}">{{ $datas['activity_name'] }}</option>
                                        @endforeach
                                    </select>
                            </div>
                        </div>
                    <div class="border-top">
                        <div class="card-body">
                            <button class="btn btn-primary" type="button" onclick="doSubmit()"><i class="fa fa-save"></i> {{ Lang::get('system.button.save',array(),config('app.locale')) }}</button>
                        </div>
                    </div>
                {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <script>
        function doSubmit() {
            var xml = document.getElementsByName("fbpmn").value;
            xml = "<![CDATA[" + xml + "]]>";
            document.getElementsByName("fbmn").value = xml;
            document.getElementById("form").submit();
        }
    </script>
    <!-- ============================================================== -->
    <!-- Data table -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
@endsection
