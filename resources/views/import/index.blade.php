@extends('layout.master',['menu'=>$menu])
@section('content')
        <!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">

    <!-- ============================================================== -->
    <!-- Data table -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        @include('layout.msg')
                        <table id="import_data" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>Nama</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                        </table>
                        <br>
                        <a href="{{ route('import.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i>{{ Lang::get('system.button.add') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Data table -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
@endsection
