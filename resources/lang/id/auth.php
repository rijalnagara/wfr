<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Nama pengguna atau kata sandi tidak terdaftar.',
    'throttle' => 'Percobaan untuk login terlalu banyak. Silahkan coba lagi dalam :seconds detik.',
    'header'=>'Form Login',
    'username.empty'=>'Masukkan nama pengguna',
    'password.empty'=>'Masukkan kata sandi',
    'username'=>'Nama Pengguna',
    'password'=>'Kata Sandi',
    'login'=>'Masuk',

];
