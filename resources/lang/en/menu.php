<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'master.data' => 'Master Data',
    'master.data.patient' => 'Patient',
    'master.data.doctor'=>'Doctor',
    'master.data.disease'=>'Disease',
    'outpatient'=>'Outpatient',
    'outpatient.registration'=>'Registration',
    'outpatient.doctor.schedule'=>'Doctor\'s Schedule',

];
