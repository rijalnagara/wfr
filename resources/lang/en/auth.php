<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Username or password do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'header'=>'Form Login',
    'username.empty'=>'Enter an username',
    'password.empty'=>'Enter an password',

];
