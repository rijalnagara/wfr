<?php
// Setting dari Kinerja.um.ac.id

return $app = [
  // SETTING JAM KERJA
  'jadwal' => [
    'regular' => [
      'jam_masuk' => '07:00:00',
      'jam_pulang' => '16:00:00',
      'jam_pulang_jumat' => '14:30:00',
      'toleransi_terlambat' => '00:30:00',
      'jam_istirahat' => '01:00:00',
      'durasi_kerja' => '08:00:00',
      'durasi_kerja_jumat' => '06:30:00'
    ],
    'ramadhan' => [
      'jam_masuk' => '07:30:00',
      'jam_pulang' => '14:30:00',
      'jam_pulang_jumat' => '15:00:00',
      'toleransi_terlambat' => '00:30:00',
      'jam_istirahat' => '01:00:00',
      'durasi_kerja' => '06:30:00',
      'durasi_kerja_jumat' => '06:30:00'
    ],
    'perpus2' => [
      'jam_masuk' => '10:00:00',
      'jam_pulang' => '19:00:00',
      'jam_pulang_jumat' => '17:30:00',
      'toleransi_terlambat' => '00:30:00',
      'jam_istirahat' => '01:00:00',
      'durasi_kerja' => '08:00:00',
      'durasi_kerja_jumat' => '06:30:00'
    ]
  ],
  
  // KATEGORI TIDAK MASUK ##
  'kategori_absen' => [
    '1' => "Ijin", 
    '2' => "Sakit", 
    '3' => "Surat Dokter", 
    '4' => "Tugas Belajar", 
    '5' => "Tugas Dinas", 
    '6' => "Tanpa Keterangan", 
    '7' => "Cuti Tahunan", 
    '8' => "Cuti Besar", 
    '9' => "Cuti Bersalin", 
    '10' => "Cuti Alasan Penting", 
    '11' => "Tugas Dinas Expert"
  ],
  // Untuk sinkronisasi. Tabel remun_tendik.presensi
  'field_kategori_absen' => [
    '1' => "i", 
    '2' => "s", 
    '3' => "sd", 
    '4' => "tb", 
    '5' => "td", 
    '6' => "tk", 
    '7' => "cth", 
    '8' => "vtb", 
    '9' => "ctm", 
    '10' => "cap", 
    '11' => "td"
  ]
];

