<?php

//This is variable is an example - Just make sure that the urls in the 'idp' config are ok.
$idp_host = env('SAML2_IDP_HOST', 'http://localhost:88/simplesaml');

return $settings = array(

    /**
     * If 'useRoutes' is set to true, the package defines five new routes:
     *
     *    Method | URI                      | Name
     *    -------|--------------------------|------------------
     *    POST   | {routesPrefix}/acs       | saml_acs
     *    GET    | {routesPrefix}/login     | saml_login
     *    GET    | {routesPrefix}/logout    | saml_logout
     *    GET    | {routesPrefix}/metadata  | saml_metadata
     *    GET    | {routesPrefix}/sls       | saml_sls
     */
    'useRoutes' => true,

    'routesPrefix' => '/saml2',

    /**
     * which middleware group to use for the saml routes
     * Laravel 5.2 will need a group which includes StartSession
     */
    'routesMiddleware' => ['saml'],

    /**
     * Indicates how the parameters will be
     * retrieved from the sls request for signature validation
     */
    'retrieveParametersFromServer' => false,

    /**
     * Where to redirect after logout
     */
    'logoutRoute' => '/',

    /**
     * Where to redirect after login if no other option was provided
     */
    'loginRoute' => '/dashboard',


    /**
     * Where to redirect after login if no other option was provided
     */
    'errorRoute' => '/',




    /*****
     * One Login Settings
     */



    // If 'strict' is True, then the PHP Toolkit will reject unsigned
    // or unencrypted messages if it expects them signed or encrypted
    // Also will reject the messages if not strictly follow the SAML
    // standard: Destination, NameId, Conditions ... are validated too.
    'strict' => true, //@todo: make this depend on laravel config

    // Enable debug mode (to print errors)
    'debug' => env('APP_DEBUG', true),

    // If 'proxyVars' is True, then the Saml lib will trust proxy headers
    // e.g X-Forwarded-Proto / HTTP_X_FORWARDED_PROTO. This is useful if
    // your application is running behind a load balancer which terminates
    // SSL.
    'proxyVars' => true,

    // Service Provider Data that we are deploying
    'sp' => array(

        // Specifies constraints on the name identifier to be used to
        // represent the requested subject.
        // Take a look on lib/Saml2/Constants.php to see the NameIdFormat supported
        'NameIDFormat' => 'urn:oasis:names:tc:SAML:2.0:nameid-format:persistent',

        // Usually x509cert and privateKey of the SP are provided by files placed at
        // the certs folder. But we can also provide them with the following parameters
        'x509cert' => env('SAML2_SP_x509',''),
        'privateKey' => env('SAML2_SP_PRIVATEKEY',''),

        // Identifier (URI) of the SP entity.
        // Leave blank to use the 'saml_metadata' route.
        'entityId' => env('SAML2_SP_ENTITYID',''),

        // Specifies info about where and how the <AuthnResponse> message MUST be
        // returned to the requester, in this case our SP.
        'assertionConsumerService' => array(
            // URL Location where the <Response> from the IdP will be returned,
            // using HTTP-POST binding.
            // Leave blank to use the 'saml_acs' route
            'url' => '',
        ),
        // Specifies info about where and how the <Logout Response> message MUST be
        // returned to the requester, in this case our SP.
        // Remove this part to not include any URL Location in the metadata.
        'singleLogoutService' => array(
            // URL Location where the <Response> from the IdP will be returned,
            // using HTTP-Redirect binding.
            // Leave blank to use the 'saml_sls' route
            'url' => '',
        ),
    ),

    // Identity Provider Data that we want connect with our SP
    'idp' => array(
        // Identifier of the IdP entity  (must be a URI)
        'entityId' => env('SAML2_IDP_ENTITYID', $idp_host . '/saml2/idp/metadata.php'),
        // SSO endpoint info of the IdP. (Authentication Request protocol)
        'singleSignOnService' => array(
            // URL Target of the IdP where the SP will send the Authentication Request Message,
            // using HTTP-Redirect binding.
            'url' => $idp_host . '/saml2/idp/SSOService.php',
        ),
        // SLO endpoint info of the IdP.
        'singleLogoutService' => array(
            // URL Location of the IdP where the SP will send the SLO Request,
            // using HTTP-Redirect binding.
            'url' => $idp_host . '/saml2/idp/SingleLogoutService.php',
        ),
        // Public x509 certificate of the IdP
        'x509cert' => 'MIIDxDCCAqygAwIBAgIJANx+r4u3ylNdMA0GCSqGSIb3DQEBBQUAMEoxCzAJBgNVBAYTAklOMQ4wDAYDVQQIEwVKQVRJTTEMMAoGA1UEBxMDTUxHMQ8wDQYDVQQKEwZVTS1ERVYxDDAKBgNVBAsTA3RpazAeFw0xODA0MDgxMzU1MTJaFw0yODA0MDcxMzU1MTJaMEoxCzAJBgNVBAYTAklOMQ4wDAYDVQQIEwVKQVRJTTEMMAoGA1UEBxMDTUxHMQ8wDQYDVQQKEwZVTS1ERVYxDDAKBgNVBAsTA3RpazCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALyGRQNEOHEmNCENKIPW/nxkWL3OaE0BSfTJ4HCLUKGS9TAMnqxs9IeY1S1jKE5nIIOdkmtZySitNJR/kjkMG0w3x8LtdYX8xIW4yk1ZLDIlr9TXrU10WeTitZosZIjwVpfsKPQQj5h9KyaCQL0iheVLaAuGBETriwMNdUNc1Yc0UD+bDHNCxRNxtBoUgFlItz37Q0NwoBGegSU6D/6F+rdEmMjeysmq2ymnTumP9riSJalovTWV9xfV6kiXbY7t0asFpgZtebKXKlYFNltx7VZLiZ4755huT2MOxHZHrVU0LLVY3BVt6Kad7biq9xy8xXZbCwUQ9Pt0YZ+oDqX8ojMCAwEAAaOBrDCBqTAdBgNVHQ4EFgQUW9RuzmNGveaHddck8zQs5+FaTkowegYDVR0jBHMwcYAUW9RuzmNGveaHddck8zQs5+FaTkqhTqRMMEoxCzAJBgNVBAYTAklOMQ4wDAYDVQQIEwVKQVRJTTEMMAoGA1UEBxMDTUxHMQ8wDQYDVQQKEwZVTS1ERVYxDDAKBgNVBAsTA3Rpa4IJANx+r4u3ylNdMAwGA1UdEwQFMAMBAf8wDQYJKoZIhvcNAQEFBQADggEBAAYfZ/eE96RF1iRxuvLfFL70tazJ+km8YFJQaKjLfg8dDjGRNmwuTVS1D5BUEqBl5bfFfmJSUeowV1NXWb7/jTU5KMj0vTG2a4fohewWR3eACL/aW0EcEdQPRYxrvokRJWa1d8gcIgH4fx4otNaOkVTjHYf0tUvRygVcBSxqnOLaCwuaIh7MA0ZSHl8xe+16qlQ7kFe1KJoWbUxbZy9wMCTP1tZeB+0qqSGeIBrCmOaGZvb/JLXTqaWQkYwrgGJ8oiXD9/6DXl3qXw0s1cgjikub34HkwQhGbWrHgjeDW/xvQeappHxBEsT+QYq4G9cGLKPRDwfOZojUI3uxs1Glqcw=',
        //'x509cert' => env('SAML2_IDP_x509', 'MIIDpjCCAo6gAwIBAgIJAOWPwXvWpzGiMA0GCSqGSIb3DQEBBQUAMEAxCzAJBgNVBAYTAmluMQ4wDAYDVQQIEwVqYXRpbTEhMB8GA1UEChMYSW50ZXJuZXQgV2lkZ2l0cyBQdHkgTHRkMB4XDTE4MDQwODE0MDUyNFoXDTI4MDQwNzE0MDUyNFowQDELMAkGA1UEBhMCaW4xDjAMBgNVBAgTBWphdGltMSEwHwYDVQQKExhJbnRlcm5ldCBXaWRnaXRzIFB0eSBMdGQwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDU7kdXWtTxJm2xXqN/l4zcWVdQQAx1vl+RY0P7BpAII19/pIFJ+sChPNJ+uNFDvqc5okyEYdlH3sLd2ERhXf+BnsOZsQzbOVEPZ0w1QtoDqKDGY09ELiPe3btH1dPp4tLmGr8AvKt8Baxlmte9p6d06A8m8wwxo4OrQ75HgZiB3Wz7m4eyf9BxfsM46pJK2qqWAFZGp/mLyYvVWcRnQrIL14RrwCVU4NetFKDXsh6qQC5sV0zYfGyjuGkaEoZX8fnLVzKhtd+w72tS7uq6Ez72hB24VPCUC5OosU5pvJlVTlZJgny4bOS4iTdKquzcL6XLThA90JDlS64NOA7QQnx1AgMBAAGjgaIwgZ8wHQYDVR0OBBYEFGENqSX1am+dg83wUraYRwMZI23qMHAGA1UdIwRpMGeAFGENqSX1am+dg83wUraYRwMZI23qoUSkQjBAMQswCQYDVQQGEwJpbjEOMAwGA1UECBMFamF0aW0xITAfBgNVBAoTGEludGVybmV0IFdpZGdpdHMgUHR5IEx0ZIIJAOWPwXvWpzGiMAwGA1UdEwQFMAMBAf8wDQYJKoZIhvcNAQEFBQADggEBAKrBPw3CXsz19F8iQXFkfnJo0V+tc4f8YfDn7ClT2zo9Wq4HBi6WO06f/EG+c2hnvOWLZv3+dQZG/HICSVSLhvnnE+TfkJoCWGKNhxbK0N3WmWQOzZJu9t7wxGFlT37U9+EdvGgQ5KRK4RXUQdBOAaf1lODn8k5qA6RqGioZ4WIfkuRqesSZkJQBti4CTXnrlIjiqFU4JEpQQlsh5w1MbtprwdDO+zYom1O84L9PYwI12KP0XlDMYJCYvQ3BuTBYeh/baPZq1GXtn4Y+gDbcXS9QBlRXOcHPQzPWdcZSbdYmuyUeDQ3/Z6kEek22dKGrrDjoLx6jvHpMgOhyF71yaYQ='),
        /*
         *  Instead of use the whole x509cert you can use a fingerprint
         *  (openssl x509 -noout -fingerprint -in "idp.crt" to generate it)
         */
        // 'certFingerprint' => '',
    ),



    /***
     *
     *  OneLogin advanced settings
     *
     *
     */
    // Security settings
    'security' => array(

        /** signatures and encryptions offered */

        // Indicates that the nameID of the <samlp:logoutRequest> sent by this SP
        // will be encrypted.
        'nameIdEncrypted' => false,

        // Indicates whether the <samlp:AuthnRequest> messages sent by this SP
        // will be signed.              [The Metadata of the SP will offer this info]
        'authnRequestsSigned' => false,

        // Indicates whether the <samlp:logoutRequest> messages sent by this SP
        // will be signed.
        'logoutRequestSigned' => false,

        // Indicates whether the <samlp:logoutResponse> messages sent by this SP
        // will be signed.
        'logoutResponseSigned' => false,

        /* Sign the Metadata
         False || True (use sp certs) || array (
                                                    keyFileName => 'metadata.key',
                                                    certFileName => 'metadata.crt'
                                                )
        */
        'signMetadata' => false,


        /** signatures and encryptions required **/

        // Indicates a requirement for the <samlp:Response>, <samlp:LogoutRequest> and
        // <samlp:LogoutResponse> elements received by this SP to be signed.
        'wantMessagesSigned' => false,

        // Indicates a requirement for the <saml:Assertion> elements received by
        // this SP to be signed.        [The Metadata of the SP will offer this info]
        'wantAssertionsSigned' => false,

        // Indicates a requirement for the NameID received by
        // this SP to be encrypted.
        'wantNameIdEncrypted' => false,

        // Authentication context.
        // Set to false and no AuthContext will be sent in the AuthNRequest,
        // Set true or don't present thi parameter and you will get an AuthContext 'exact' 'urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransport'
        // Set an array with the possible auth context values: array ('urn:oasis:names:tc:SAML:2.0:ac:classes:Password', 'urn:oasis:names:tc:SAML:2.0:ac:classes:X509'),
        'requestedAuthnContext' => true,
    ),

    // Contact information template, it is recommended to suply a technical and support contacts
    'contactPerson' => array(
        'technical' => array(
            'givenName' => 'name',
            'emailAddress' => 'no@reply.com'
        ),
        'support' => array(
            'givenName' => 'Support',
            'emailAddress' => 'no@reply.com'
        ),
    ),

    // Organization information template, the info in en_US lang is recomended, add more if required
    'organization' => array(
        'en-US' => array(
            'name' => 'Name',
            'displayname' => 'Display Name',
            'url' => 'http://url'
        ),
    ),

    /* Interoperable SAML 2.0 Web Browser SSO Profile [saml2int]   http://saml2int.org/profile/current

       'authnRequestsSigned' => false,    // SP SHOULD NOT sign the <samlp:AuthnRequest>,
                                          // MUST NOT assume that the IdP validates the sign
       'wantAssertionsSigned' => true,
       'wantAssertionsEncrypted' => true, // MUST be enabled if SSL/HTTPs is disabled
       'wantNameIdEncrypted' => false,
    */

);
